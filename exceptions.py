class UnAuthorizationException(Exception):
    code = 401

    def __init__(self, message, code=401, *args):
        super().__init__(args)
        self.message = message
        self.code = code


class FinanceServiceException(Exception):
    code = 401

    def __init__(self, message, code=401, *args):
        super().__init__(args)
        self.message = message
        self.code = code
