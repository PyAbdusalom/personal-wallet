class User:
    counter = 0

    def __init__(self,
                 username: str,
                 full_name: str,
                 password: str,
                 current_amount: float):
        User.counter += 1
        self.id = User.counter
        self.Username = username
        self.FullName = full_name
        self.Password = password
        self.CurrentAmount = current_amount


class Wallet:
    counter = 0

    def __init__(self,
                 user: User,
                 date: str,
                 category: str,
                 amount: str,
                 description: str,
                 ):
        Wallet.counter += 1
        self.id = Wallet.counter
        self.User = user
        self.Date = date
        self.Category = category
        self.Amount = amount
        self.Description = description
