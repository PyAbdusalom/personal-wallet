from uuid import uuid4
from colorama import Fore, Style


def generate_id():
    return str(uuid4())


def print_menu(message):
    print(Fore.BLUE, message, Style.RESET_ALL)


def print_error(message):
    print(Fore.RED, message, Style.RESET_ALL)


def print_info(message):
    print(Fore.CYAN, message, Style.RESET_ALL)


def print_success(message):
    print(Fore.GREEN, message, Style.RESET_ALL)
