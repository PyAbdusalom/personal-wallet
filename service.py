import json

from exceptions import UnAuthorizationException
from models import Wallet, User
from datetime import date

from utils import print_info

session_user: dict = None


class AuthorService:
    users = []

    def __init__(self):
        self.__get_data()

    def __get_data(self):
        try:
            with open("users.json", "r") as f:
                data = json.load(f)
        except FileNotFoundError:
            with open("users.json", "x") as f:
                json.dump([], f)
                data = []
        self.users = data

    def login(self, username: str, password: str):
        loginuser: dict = None
        for user in self.users:
            if user["Username"] == username:
                loginuser = user
                break
        else:
            raise UnAuthorizationException("Username not Found")

        if loginuser.get("Password") != password:
            raise UnAuthorizationException("User password incorrect")
        return loginuser

    def register(self, **kwargs):
        username = kwargs.get("username")
        if username is None:
            raise Exception("Username can not be None")
        password = kwargs.get("password")
        if password is None:
            raise Exception("Password can not be None")

        user = User(
            username=username,
            password=password,
            full_name=kwargs.get('full_name'),
            current_amount=0
        )
        self.users.append(user.__dict__)
        self.__commit()

    def __commit(self):
        with open("users.json", "w") as f:
            json.dump(self.users, f, indent=4)

    def check_username_unique(self):
        pass


class FinanceService:
    wallett = []

    def __init__(self):
        self.data = self.__get_data()

    def __get_data(self):
        try:
            with open("wallet.json", "r") as f:
                data = json.load(f)
        except FileNotFoundError:
            with open("wallet.json", "x") as f:
                json.dump([], f)
                data = []
        self.wallet = data
        return data

    def __commit(self):
        with open("wallet.json", "w") as f:
            json.dump(self.wallett, f, indent=4)

    def add_text(self, user, category, amount, description):
        wallet = Wallet(
            user=user,
            date=date.today().strftime('%Y-%m-%d'),
            category=category,
            amount=amount,
            description=description
        )
        self.wallett.append(wallet.__dict__)
        self.__commit()

    def show_current_balance(self, user):
        print_info(f"Your balance {user['CurrentAmount']}")

    def show_all_text(self, user):
        for i in self.data:
            if i['User'] == user:
                print("Id:", i['id'])
                print("Date:", i["Date"])
                print("Category:", i["Category"])
                print("Amount:", i["Amount"])
                print("Description:", i["Description"])

    def show_only_consumption(self, user):
        for i in self.data:
            if i['User'] == user:
                if i['Category'] == 'Harajat':
                    print("Id:", i['id'])
                    print("Date:", i["Date"])
                    print("Category:", i["Category"])
                    print("Amount:", i["Amount"])
                    print("Description:", i["Description"])

    def show_only_income(self, user):
        for i in self.data:
            if i['User'] == user:
                if i['Category'] == 'Daromad':
                    print("Id:", i['id'])
                    print("Date:", i["Date"])
                    print("Category:", i["Category"])
                    print("Amount:", i["Amount"])
                    print("Description:", i["Description"])

    def update_description(self, user_id, wallet_id, new_description):
        for transaction in self.data:
            if transaction['User']['id'] == user_id:
                transaction['Description'] = new_description
                self.wallett.append(transaction)
                self.__commit()
                print_info("Description updated successfully.")
                return

    def search_transactions(self, user=None, date=None, category=None, amount=None):
        found_transactions = []

        for transaction in self.wallett:
            if user is not None and transaction['user'] != user:
                continue
            if date is not None and transaction['date'] != date:
                continue
            if category is not None and transaction['category'] != category:
                continue
            if amount is not None and transaction['amount'] != amount:
                continue
            found_transactions.append(transaction)

        if found_transactions:
            for i, transaction in enumerate(found_transactions, 1):
                print(f"Transaction {i}:")
                print("Date:", transaction["date"])
                print("Category:", transaction["category"])
                print("Amount:", transaction["amount"])
                print("Description:", transaction["description"])
                print("-------------------------")
        else:
            print_info("No transactions found matching the search criteria.")
