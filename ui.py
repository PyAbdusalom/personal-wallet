from exceptions import FinanceServiceException, UnAuthorizationException
from service import AuthorService, FinanceService
from utils import print_error, print_success, print_menu
from utils import print_info

session_user: dict = None
auth = AuthorService()
finance = FinanceService()


def menu():
    print_info("__________________________")
    if session_user is None:
        print_menu("Login       ----> 1")
        print_menu("Register    ----> 2")
    else:
        print_menu("Show Current Balance      ----> 1")
        print_menu("Add Text       ----> 2")
        print_menu("Show All Text  ----> 3")
        print_menu("Show only Consumption   ----> 4")
        print_menu("Show only Income   ----> 5")
        print_menu("Alter Description   ----> 6")
        print_menu("Search   ----> 7")
        print_menu("Log Out   ----> 8")
    print_menu("Quit   ----> q")
    choice = input("> ?")
    if choice == "q":
        return
    if session_user:
        user_menu(choice)
    else:
        auth_menu(choice)
    menu()


def auth_menu(choice):
    global session_user
    try:
        match choice:
            case "1":
                username = input("Username: ")
                password = input("Password: ")
                session_user = auth.login(username=username, password=password)
                print_success("User Login")
            case "2":
                full_name = input("Full Name: ")
                username = input("Username: ")
                password = input("Password: ")
                auth.register(full_name=full_name,
                              password=password,
                              username=username,
                              )
                print_success("User registered")
    except UnAuthorizationException as e:
        print_error(e.message)


def user_menu(choice):
    global session_user
    try:
        match choice:
            case "1":
                finance.show_current_balance(session_user)
            case "2":
                print_info("Harajat ---> 1")
                print_info("Daromad ---> 2")
                ch = input(">")
                if ch == 1:
                    category = "Harajat"
                else:
                    category = "Daromad"
                amount = input("Miqdor: ")
                description = input("Tavsif: ")

                finance.add_text(session_user, category, amount, description)
            case "3":
                finance.show_all_text(session_user)
            case "4":
                finance.show_only_consumption(session_user)
            case "5":
                finance.show_only_income(session_user)
            case "6":
                finance.show_all_text(session_user)
                wallet_id = input("Enter the ID of the transaction you want to alter: ")
                new_description = input("Enter the new description: ")
                finance.update_description(session_user['id'], wallet_id, new_description)
            case "7":
                date = input("Enter date (optional, format: YYYY-MM-DD): ").strip()
                category = input("Enter category (optional): ").strip()
                amount = input("Enter amount (optional): ").strip()

                if amount:
                    try:
                        amount = float(amount)
                    except ValueError:
                        print_error("Invalid amount format. Please enter a valid number.")
                        return

                finance.search_transactions(user=session_user, date=date, category=category, amount=amount)

            case "8":
                session_user = None

    except FinanceServiceException as e:
        print_error(e.message)


if __name__ == '__main__':
    menu()
